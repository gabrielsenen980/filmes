//
//  TableViewCell.swift
//  filmes
//
//  Created by COTEMIG on 27/10/22.
//

import UIKit


class TableViewCell: UITableViewCell {
    @IBOutlet weak var imagem: UIImageView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
