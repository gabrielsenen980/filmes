//
//  ViewController.swift
//  filmes
//
//  Created by COTEMIG on 27/10/22.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftUI

struct Personagen: Decodable{
    let name: String
    let actor : String
    let image : String
}

class ViewController: UIViewController,UITableViewDataSource {
    
    var Personagenlist : [Personagen]?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Personagenlist?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        
                let personagen = Personagenlist![indexPath.row]
                
                cell.label2.text = personagen.name
                cell.label1.text = personagen.actor
                cell.imagem.kf.setImage(with: URL(string: personagen.image))
                
                return cell
    }
    
    @IBOutlet weak var TableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        data()
        TableView.dataSource = self

    }
    private func data(){
            AF.request("https://hp-api.herokuapp.com/api/characters")
                .responseDecodable(of: [Personagen].self){
                response in
                    if let Personagen_list = response.value{
                        self.Personagenlist = Personagen_list
                }
                    self.TableView.reloadData()
                }
        
    }

}

